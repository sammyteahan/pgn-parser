"""
Utils
"""
import csv

from game import GAME_FIELD_NAMES # pylint: disable=W0403

def write_csv(games):
    """
    :param games: list
    :return: None
    """
    with open('outfile.csv', 'w') as outfile:
        writer = csv.writer(outfile, delimiter=',')
        writer.writerow(GAME_FIELD_NAMES)
        writer.writerows(games)

def create_empty_csv():
    """
    creates a csv file with headers from our named tuple
    """
    with open('outfile.csv', 'w') as data_file:
        writer = csv.writer(data_file, delimiter=',')
        writer.writerow(GAME_FIELD_NAMES)

def append_to_csv(games):
    """
    opens and appends to our existing csv file
    """
    with open('outfile.csv', 'a') as data_file:
        writer = csv.writer(data_file, delimiter=',')
        writer.writerows(games)
