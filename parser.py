"""
PGN parser
"""

import re

from patterns import FULL_GAME_PATTERN # pylint: disable=W0403
from game import as_game # pylint: disable=W0403

def parse_pgn_file(contents):
    """
    :param contents: string
    :return: list Game
    """
    games = re.findall(FULL_GAME_PATTERN, contents, re.X|re.M)
    return [as_game(game) for game in games]
