"""
Collection of regular expressions

TODO handle different time controls for `time_control`
TODO handle game drawn case for `result`
"""

FULL_GAME_PATTERN = r'''
    (?P<event>\[Event\s\"\w+\s\w+\"\])\n
    (?P<site>\[Site\s\"\w+\D\w+\"\])\n
    (?P<date>\[Date\s\"\d{4}\D{1}\d{2}\D{1}\d{2}\"\])\n
    (?P<round>\[Round\s\"\D{1}\"\])\n
    (?P<white>\[White\s\"\w+\"\])\n
    (?P<black>\[Black\s\"\w+\"\])\n
    (?P<record>\[Result\s\"\d{1}\D{1}\d{1}\"\])\n
    (?P<white_elo>\[WhiteElo\s\"\d{2,4}\"\])\n
    (?P<black_elo>\[BlackElo\s\"\d{2,4}\"\])\n
    (?P<time_control>\[TimeControl\s\"\d{3}\D{1}?\d{1}?\"\])\n
    (?P<end_time>\[EndTime\s\"\d{2}\D{1}\d{2}\D{1}\d{2}\s\w{3}\"\])\n
    (?P<result>\[Termination\s\"\w+\s\w+\s\w+\s\w+\"\])\n
'''
