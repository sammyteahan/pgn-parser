"""
Game
"""
import re
from collections import namedtuple

GAME_FIELD_NAMES = ['event', 'site', 'date', 'round', 'white', 'black',
                    'record', 'white_elo', 'black_elo', 'time_control',
                    'end_time', 'result']

Game = namedtuple('Game', GAME_FIELD_NAMES)

def strip_brackets(str_val):
    """
    :param: str_val
    :return: str_val
    """
    return re.sub(r'(\[|\])', '', str_val)

def remove_first_word(str_val):
    """
    :param: str_val
    :return: string
    """
    return str_val.split(' ', 1)[1]

def remove_quotes(str_val):
    """
    :param: str_val
    :return: string
    """
    return re.sub(r'\"', '', str_val)

def clean_value(str_val):
    """
    A small 'cleaning pipeline' for the given list value

    :param: str_val
    :return: string
    """
    cleaned = strip_brackets(str_val)
    cleaned = remove_first_word(cleaned)
    cleaned = remove_quotes(cleaned)
    return cleaned

def as_game(game):
    """
    :param: tuple
    :return: Game
    """
    values = [clean_value(val) for val in list(game)]
    return Game(*values)
