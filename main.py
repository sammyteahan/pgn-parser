"""
PGN Parser

Currently this regular expression only handles 2-1 time control games
and a path to a single file rather than a directory
"""
import os
import sys
import argparse
import pathlib # pylint: disable=E0401

from parser import parse_pgn_file # pylint: disable=W0403
from utils import create_empty_csv, append_to_csv # pylint: disable=W0403

def get_file_contents(file_name):
    """
    :param file_name: string
    :return: string contents of file
    """
    path = pathlib.Path('data/{}'.format(file_name))
    open_file = open(path)
    return open_file.read()

def get_games_from_directory(directory):
    """
    :param directory: string name of directory
    :return: void
    """
    for file_name in os.listdir(directory):
        contents = get_file_contents(file_name)
        games = parse_pgn_file(contents)
        append_to_csv(games)

def get_kwargs():
    """
    get command line args
    """
    parser = argparse.ArgumentParser(description='PGN Parser',
                                     epilog='Note: Run with python3')
    parser.add_argument('--path',
                        help='path to your .pgn file(s)',
                        required=True)
    parser.add_argument('--handle',
                        help='your user handle',
                        required=True)
    args = parser.parse_args()

    return vars(args)

def main():
    """
    entry function
    """
    create_empty_csv()
    kwargs = get_kwargs()
    get_games_from_directory(kwargs.get('path'))


if __name__ == '__main__':
    sys.exit(main())
