"""
Analytics
"""

def basic_analytics(**kwargs):
    """
    :param games: list tuple
    :param user_handle: string
    :return: void
    """
    print('{} games parsed'.format(len(kwargs['games']))) # pylint: disable=C0325
    wins = 0
    losses = 0
    for game in kwargs['games']:
        if '{} won'.format(kwargs['user_handle']) in game.result:
            wins += 1
        else:
            losses += 1
    total = wins + losses
    win_percentage = (wins / total) * 100

    print('{} wins, {} losses.'.format(wins, losses)) # pylint: disable=C0325
    print('winning percentage: {}%'.format(round(win_percentage, 2))) # pylint: disable=C0325
